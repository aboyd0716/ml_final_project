import tkinter as tk
from tkinter import *
import re
import webbrowser
import sentiment
import keywords
import nlp
import os
import sys
import warnings

def app_create():
    global app
    # create window object
    app = tk.Tk()

    # window title and size
    app.title('Movie Suggestions')

    # add image to the right side
    src = tk.PhotoImage(file='assets/video-camera.png') #https://www.flaticon.com/authors/vectors-market
    image = tk.Label(app, image = src)
    image.grid(row = 0, column = 2, rowspan = 16, columnspan = 2, padx =(20,10), pady = 10)

    # left side header
    title = tk.Label(app, justify = tk.CENTER, text = 'Movie Matcher',
                    font = ('Helvetica', 24, 'bold'))
    title.grid(row = 0, columnspan = 2, pady = (10,0))

    # Left side information
    info = """
    Our movie suggestion app uses machine learning 
    to predict a movie you would like. Enter a couple
    of keywords or phrases to see what your next
    movie should be!"""
    info = tk.Label(app, justify = tk.CENTER,text = info, font = ('Helvetica', 11))
    info.grid(row = 1, columnspan = 2, pady = 0, padx = 10, sticky = N)

    # Left Side Input
    global key_label
    key_label = tk.Label(app, text = 'Tell Us  What You Like:', font = ('Helvetica', 13, 'bold'))
    key_label.grid(row = 2, columnspan = 2, padx = 10)
    global key_entry
    key_entry = tk.Text(app, height = 3, width = 40, padx = 2, pady = 2)
    key_entry.grid(row = 3, columnspan = 2, padx = 10)
    # Button
    global button
    button = tk.Button(app, text = "Enter", width = 30, borderwidth = 3,
                    command = lambda: get_input(),
                    font = ('Helvetica', 11))
    button.grid(row = 4, columnspan = 2, pady = (0,10))

    # start the app
    app.mainloop()

def restart():
    app.destroy()
    app_create()
    
def callback(url):
    webbrowser.open_new(url)

def quit():
    app.destroy()

def get_input():
    # get input text -> convert to keyword list
    text = key_entry.get("1.0", END)
    key_entry.delete('1.0', END)

    # hide all of the input etc
    key_label.destroy()
    key_entry.destroy()
    button.destroy()

    #  print header for the movies
    m_text = tk.Label(app,text = "Movies", justify = tk.CENTER, 
                    font = ('Helvetica',16, 'bold'))
    m_text.grid(row = 2, columnspan = 2)

    # generate movies based on the keywords
    key_string = nlp.process_string_lemma(text)
    print(key_string)
    movie_suggestions = keywords.main(key_string)
    print(movie_suggestions)
    if 'Mission: Impossible - Rogue Nation' in movie_suggestions:
        movie_suggestions.remove('Mission: Impossible - Rogue Nation')
    if 'The Last of the Mohicans' in movie_suggestions:
        movie_suggestions.remove('The Last of the Mohicans')
    if 'Project X' in movie_suggestions:
        movie_suggestions.remove('Project X')
    movie_list = sentiment.main(movie_suggestions)
    movies = sorted(movie_list, key = lambda i: i['sentiment'], reverse=True)
    

    count = 1
    label_list = []
    url_list = []
    for movie in movies:
        label_list.append(tk.Label(app, text = "{}: {}".format(count, movie['title']),
                            font = ('Helvetica', 11),
                            justify = tk.LEFT,
                            name = '{}'.format(count)))
        url_list.append(movie['url'])
        count += 1
    
    count = 0
    row = 3
    for label in label_list:
        if(str(label)) == '.1':
            label.bind("<Button-1>",lambda event: callback(url_list[0]))
        elif(str(label)) == '.2':
            label.bind("<Button-1>",lambda event: callback(url_list[1]))
        elif(str(label)) == '.3':
            label.bind("<Button-1>",lambda event: callback(url_list[2]))
        elif(str(label)) == '.4':
            label.bind("<Button-1>",lambda event: callback(url_list[3]))
        elif(str(label)) == '.5':
            label.bind("<Button-1>",lambda event: callback(url_list[4]))
        elif(str(label)) == '.6':
            label.bind("<Button-1>",lambda event: callback(url_list[5]))
        elif(str(label)) == '.7':
            label.bind("<Button-1>",lambda event: callback(url_list[6]))
        elif(str(label)) == '.8':
            label.bind("<Button-1>",lambda event: callback(url_list[7]))
        elif(str(label)) == '.9':
            label.bind("<Button-1>",lambda event: callback(url_list[8]))
        label.grid(row = row, columnspan = 2)
        count += 1
        row += 1
    
    new_movie_btn = tk.Button(app, text = "New Movie", width = 15, borderwidth = 3,
                            command = lambda: restart(),
                            font = ('Helvetica', 11))
    new_movie_btn.grid(row = row, column = 0)
    quit_btn = tk.Button(app, text = "Quit", width = 15, borderwidth = 2,
                        command = lambda: quit(),
                        font = ('Helvetica', 11))
    quit_btn.grid(row = row, column = 1)


if __name__ == '__main__':    
    app_create()
