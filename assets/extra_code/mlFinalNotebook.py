import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
df = pd.read_csv("movie_dataset.csv")
#df = df.iloc[:10000] this is what creates a subset of the dataframe with only 10000 rows
features = ['keywords','cast','genres','director']
def combine_features(row):
    return row['keywords'] +" "+row['cast']+" "+row["genres"]+" "+row["director"]
for feature in features:
    df[feature] = df[feature].fillna('')
df["combined_features"] = df.apply(combine_features,axis=1)

def get_title_from_index(index):
    titles = []
    for i in index:
      titles.append(df[df.index == i]["title"].values[0])
    return titles
def get_cosine_sim(strs):
  
  vectors = [t for t in get_vectors(strs)]
  return cosine_similarity(vectors)
def get_vectors(strs):
  text = [t for t in strs]
  vectorizer = CountVectorizer(text)
  vectorizer.fit(text)
  return vectorizer.transform(text).toarray()

strings = list(df["combined_features"])
an_input = "mammoth ice snow sloth continent squirrel acorn"
length = len(strings)
strings.append(an_input)
similarity = get_cosine_sim(strings)
#index = np.where(similarity[length][:length-1] == np.amax(similarity[length][:length-1]))
#index = np.where(similarity[length][:length-1] > 0.15)
index = similarity[length][:length-1].argsort()[-10:][::-1]
print(index)
print(similarity[length][index])
print(get_title_from_index(index))

#setup script must be run at least once
import tensorflow as tf
import numpy as np
import tensorflow.keras as keras
from tensorflow.keras.datasets import imdb
from tensorflow.keras.models import load_model
from bs4 import BeautifulSoup
import requests
import pandas as pd
from tensorflow.keras.preprocessing import sequence

def get_url(title, df = pd.read_csv('data.tsv', sep='\t', header=0)):
  tt = df.loc[df['title'] == title, 'titleId'].iloc[0]
  url = "https://www.imdb.com/title/"+ tt +"/reviews?ref_=tt_ql_3"  
  return url

def get_sentiment(tt, index = imdb.get_word_index(), new_net = load_model("movieRNNmodel.h5"), maxlen = 300, n = 40000):
  resp = requests.get(url)
  soup = BeautifulSoup(resp.text)
  llist = soup.find_all('div',{'class':'text show-more__control'})
  count = 0;
  sumSentiment = 0;
  for x in llist:
    x = x.text
    x = ''.join(e for e in x if (e.isalnum() or e==' '))
    new_x = x.split(' ')
    newest_x = []
    newest_x.append([index.get(i,0) + 3 for i in new_x])
    for i in range(len(newest_x[0])):
      if (newest_x[0][i] >= n):
        newest_x[0][i] = 0
    newest_x = sequence.pad_sequences(newest_x,maxlen=maxlen) 
    sumSentiment += new_net.predict(newest_x)
    count+=1
  return sumSentiment/count


new_net = load_model("movieRNNmodel.h5")
df = pd.read_csv('data.tsv', sep='\t', header=0)
index = imdb.get_word_index()
maxlen = 300 # Only use sentences up to this many words
n = 40000 # Only use the most frequent n words

#switch out data for the actual movie titles
data = ["Ice Age: The Meltdown", "Ice Age: Dawn of the Dinosaurs", "Hard Candy", "Youth in Revolt", 'The Thing']
output = []
for i in data:
  url = get_url(i,df)
  sentiment = get_sentiment(url, index, new_net, maxlen, n)
  print(sentiment[0][0])
  diction = {
      "title":i,
      "sentiment": sentiment[0][0],
      "url": url[:-21]
  }
  output.append(diction)

print(output)