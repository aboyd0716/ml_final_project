import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

def main(input_string):
    df = pd.read_csv("assets/movie_dataset.csv")
    #df = df.iloc[:10000] this is what creates a subset of the dataframe with only 10000 rows
    features = ['keywords','cast','genres','director']
    def combine_features(row):
        return row['keywords'] +" "+row['cast']+" "+row["genres"]+" "+row["director"]
    for feature in features:
        df[feature] = df[feature].fillna('')
    df["combined_features"] = df.apply(combine_features,axis=1)
    def get_title_from_index(index):
        titles = []
        for i in index:
            titles.append(df[df.index == i]["title"].values[0])
        return titles
    def get_cosine_sim(strs):
        vectors = [t for t in get_vectors(strs)]
        return cosine_similarity(vectors)
    
    def get_vectors(strs):
        text = [t for t in strs]
        vectorizer = CountVectorizer(text)
        vectorizer.fit(text)
        return vectorizer.transform(text).toarray()
    strings = list(df["combined_features"])
    an_input = input_string
    length = len(strings)
    strings.append(an_input)
    similarity = get_cosine_sim(strings)
    #index = np.where(similarity[length][:length-1] == np.amax(similarity[length][:length-1]))
    #index = np.where(similarity[length][:length-1] > 0.15)
    index = similarity[length][:length-1].argsort()[-10:][::-1]
    return(get_title_from_index(index))