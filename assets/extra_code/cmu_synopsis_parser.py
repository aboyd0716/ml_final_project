import spacy
import numpy as np
import pandas as pd
from spacy.symbols import *
from spacy.matcher import Matcher
import string
from spacy.lang.en.stop_words import STOP_WORDS
from spacy.lang.en import English

file_object = open(r"plot_summaries.txt", 'r')
summaries = file_object.readlines()

movies_dict = {}
synopses_arr = []

for x in summaries:
  new_entry = []
  new_id = []
  switch = False
  for y in x:
    char = y
    if char == '\t':
      switch = True
      continue
    if switch == False:
      new_id.append(char)
    else:
      new_entry.append(char)
  new_ID = "".join(new_id)
  new_ENTRY = "".join(new_entry).rstrip()
  movies_dict[new_ID] = new_ENTRY
  synopses_arr.append(new_ENTRY)

nlp = spacy.load("en_core_web_md")

stop_words = spacy.lang.en.stop_words.STOP_WORDS
parser = English()

punctuations = string.punctuation

articles = ['the', 'a', 'an']
subject_pronouns = ['i', 'you', 'he', 'she', 'it', 'they']
object_pronouns = ['me', 'you', 'him', 'her', 'it']
possessive_pronouns = ['my', 'mine', 'your', 'yours', 'his', 'her', 'hers', 'its']
interrogative_pronouns = ['who', 'whom', 'whose', 'what', 'which']
indefinite_pronouns = ['another', 'each', 'everything', 'nobody', 'either', 'someone']
randi_pronouns = ['myself', 'yourself', 'himself', 'herself', 'itself']
demonstrative_pronouns = ['this', 'that']

articles_upper = []
subject_pronouns_upper = []
object_pronouns_upper = []
possessive_pronouns_upper = []
interrogative_pronouns_upper = []
indefinite_pronouns_upper = []
randi_pronouns_upper = []
demonstrative_pronouns_upper = []

for x in articles:
  articles_upper.append(x.capitalize())

for x in subject_pronouns:
  subject_pronouns_upper.append(x.capitalize())

for x in object_pronouns:
  object_pronouns_upper.append(x.capitalize())

for x in possessive_pronouns:
  possessive_pronouns_upper.append(x.capitalize())

for x in interrogative_pronouns:
  interrogative_pronouns_upper.append(x.capitalize())

for x in indefinite_pronouns:
  indefinite_pronouns_upper.append(x.capitalize())

for x in randi_pronouns:
  randi_pronouns_upper.append(x.capitalize())

for x in demonstrative_pronouns:
  demonstrative_pronouns_upper.append(x.capitalize())

def spacy_tokenizer(sentence):
    
    mytokens = parser(sentence)
    mytokens = [ word.lemma_.strip() for word in mytokens if word.lemma_ != "-PRON-" ]

    mytokens = [ word for word in mytokens if word not in stop_words and word not in punctuations]
    mytokens = [ word for word in mytokens if word not in articles and word not in articles_upper]
    mytokens = [ word for word in mytokens if word not in subject_pronouns and word not in subject_pronouns_upper]
    mytokens = [ word for word in mytokens if word not in object_pronouns and word not in object_pronouns_upper]
    mytokens = [ word for word in mytokens if word not in possessive_pronouns and word not in possessive_pronouns_upper]
    mytokens = [ word for word in mytokens if word not in interrogative_pronouns and word not in interrogative_pronouns_upper]
    mytokens = [ word for word in mytokens if word not in indefinite_pronouns and word not in indefinite_pronouns_upper]
    mytokens = [ word for word in mytokens if word not in randi_pronouns and word not in randi_pronouns_upper]
    mytokens = [ word for word in mytokens if word not in demonstrative_pronouns and word not in demonstrative_pronouns_upper]

    return mytokens
def ultimate_parse(ultimate_dictionary):

  i = 0
  for key in ultimate_dictionary:
    print(i)
    i += 1
    syn = nlp(ultimate_dictionary[key])

    sents_list = []
    parsed_words_arr = []
    final_words_arr = []

    for sent in syn.sents:
      sents_list.append(sent.text)

    for x in sents_list:
      parsed_words_arr.append(spacy_tokenizer(x))

    for arr in parsed_words_arr:
      for x in arr:
        final_words_arr.append(x)

    final_words = list(set(final_words_arr))
    ultimate_dictionary[key] = final_words

  return ultimate_dictionary     

result = ultimate_parse(movies_dict)

import pandas as pd
df_movies.to_csv(r'movie_keywords.csv')