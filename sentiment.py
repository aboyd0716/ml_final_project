import pandas as pd
import numpy as np
import tensorflow as tf
import numpy as np
import tensorflow.keras as keras
from tensorflow.keras.datasets import imdb
from tensorflow.keras.models import load_model
from bs4 import BeautifulSoup
import requests
import pandas as pd
from tensorflow.keras.preprocessing import sequence

def main(movie_titles):
  #setup script must be run at least once
  import tensorflow as tf
  import numpy as np
  import tensorflow.keras as keras
  from tensorflow.keras.datasets import imdb
  from tensorflow.keras.models import load_model
  from bs4 import BeautifulSoup
  import requests
  import pandas as pd
  from tensorflow.keras.preprocessing import sequence

  def get_url(title, df = pd.read_csv('assets/data.tsv', sep='\t', header=0)):
    tt = df.loc[df['title'] == title, 'titleId'].iloc[0]
    url = "https://www.imdb.com/title/"+ tt +"/reviews?ref_=tt_ql_3"  
    return url

  def get_sentiment(tt, index = imdb.get_word_index(), new_net = load_model("assets/movieRNNmodel.h5"), maxlen = 300, n = 40000):
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text)
    llist = soup.find_all('div',{'class':'text show-more__control'})
    count = 0
    sumSentiment = 0
    for x in llist:
      x = x.text
      x = ''.join(e for e in x if (e.isalnum() or e==' '))
      new_x = x.split(' ')
      newest_x = []
      newest_x.append([index.get(i,0) + 3 for i in new_x])
      for i in range(len(newest_x[0])):
        if (newest_x[0][i] >= n):
          newest_x[0][i] = 0
      newest_x = sequence.pad_sequences(newest_x,maxlen=maxlen) 
      sumSentiment += new_net.predict(newest_x)
      count+=1
    return sumSentiment/count


  new_net = load_model("assets/movieRNNmodel.h5")
  df = pd.read_csv('assets/data.tsv', sep='\t', header=0)
  index = imdb.get_word_index()
  maxlen = 300 # Only use sentences up to this many words
  n = 40000 # Only use the most frequent n words

  data = movie_titles  
  output = []
  for i in data:
    url = get_url(i,df)
    sentiment = get_sentiment(url, index, new_net, maxlen, n)
    print(sentiment[0][0])
    diction = {
        "title":i,
        "sentiment": sentiment[0][0],
        "url": url[:-21]
    }
    output.append(diction)

  return(output)