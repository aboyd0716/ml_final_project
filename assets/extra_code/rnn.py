import tensorflow as tf
import numpy as np
import tensorflow.keras as keras
from tensorflow.keras import Sequential
from tensorflow.keras.preprocessing import sequence
from tensorflow.keras.layers import SimpleRNN, GRU, LSTM, Embedding, Dense, Input
from tensorflow.keras.datasets import imdb
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from sklearn.datasets import make_classification

"""
    Creation of the RNN
"""
maxlen = 300 # Only use sentences up to this many words
n = 40000 # Only use the most frequent n words
(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=n)
X = np.concatenate((x_train, x_test), axis=0)
y = np.concatenate((y_train, y_test), axis=0)
X = sequence.pad_sequences(X,maxlen=maxlen)

def build_net():
  lstm_out = 64
  embedding_dim = 32
  drop = 0.1
  lstm_layers = [Embedding(n,embedding_dim, name="embedding_in"),
                 LSTM(lstm_out,return_sequences=True, dropout=drop, recurrent_dropout=drop, name="lstm_1"),
                 LSTM(lstm_out,return_sequences=True, dropout=drop, recurrent_dropout=drop, name="lstm_2"),
                 LSTM(lstm_out, dropout=0.1, recurrent_dropout=drop, name="lstm_3"),
                 Dense(1, activation="sigmoid", name="dense_out")]

  my_lstm = Sequential(lstm_layers)
  my_lstm.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
  print(my_lstm.summary())
  return my_lstm

new_net = build_net()
new_net.fit(X,y,epochs=3,batch_size=64)
new_net.save("movieRNN.h5")

"""
    RNN Validation
"""
neural_network = KerasClassifier(build_fn=build_net, 
                                 epochs=3, 
                                 batch_size=64)

cross_val_score(neural_network, X, y, cv=5)

test = ["I love this movie", "I hate this movie", "this movie is awful", "this movie is great"]

test = ["I can't believe this, watched from the start till the end and everything is just felt flat, Kristen Stewart trying so hard to be funny, (and fail), the opener is so boring, dialog line so generic, story is weak and mainstream, action scene is boring. big names like Sr. Patrick Stewart & Djimon Hounsou not effective to the movie. I just can Feel the movie man... the good point of this movie are just the girls are pretty, that's all. nb: best scene : last / end of the movie (before credit)",
        "Not a massive fan of motor racing or motor racing movies but i did enjoy this. The major asset for the film is the cast, Matt Damon is very good but the real star of this is the fantastic Christian Bale, a superb performance from a great actor. The story of an unsung British hero, a very modest and a very real hero, he did really drive a tank in WW2, is a tale very much worth telling. It is long bit flies by - go see.",
        "This is the worst movie I have ever seen",
        "This is the best movie I have ever seen"]
for i in range(len(test)):
  test[i] = ''.join(e for e in test[i] if (e.isalnum() or e==' '))
test2 = [i.split(' ') for i in test[:]]
testing = []
for x in range(len(test2)):
  testing.append([index.get(i,0) + 3 for i in test2[x]])
testing = sequence.pad_sequences(testing,maxlen=maxlen)
print(new_net.predict_classes(testing))

print(index.get("flabbergasteded"))

test = ["I can't believe this, watched from the start till the end and everything is just felt flat, Kristen Stewart trying so hard to be funny, (and fail), the opener is so boring, dialog line so generic, story is weak and mainstream, action scene is boring. big names like Sr. Patrick Stewart & Djimon Hounsou not effective to the movie. I just can Feel the movie man... the good point of this movie are just the girls are pretty, that's all. nb: best scene : last / end of the movie (before credit)"]
for i in range(len(test)):
  print(type(test))
  test[i] = ''.join(e for e in test[i] if (e.isalnum() or e==' '))
test2 = [i.split(' ') for i in test[:]]
print(test2)
testing = []
for x in range(len(test2)):
  testing.append([index.get(i,0) + 3 for i in test2[x]])
testing = sequence.pad_sequences(testing,maxlen=maxlen)
testing.shape
index = imdb.get_word_index()
print(index.get("love"))
testing = [index.get(i) for i in test[0].split(' ')] 
print(testing)
reverse_index = dict([(value, key) for (key, value) in index.items()]) 
decoded = " ".join( [reverse_index.get(i-3, '#') for i in testing[0]])
print(decoded)
print(new_net.predict_classes(testing))

"""
    Web Scraper Code
"""
from tensorflow.keras.models import load_model
new_net = load_model("movieRNNmodel.h5")

import pandas as pd
df = pd.read_csv('data.tsv', sep='\t', header=0)
df.describe
title = "The Lord of the Rings: The Return of the King"
tt = df.loc[df['title'] == title, 'titleId'].iloc[0]
print(tt)

from bs4 import BeautifulSoup
import requests
url = "https://www.imdb.com/title/"+ tt +"/reviews?ref_=tt_ql_3"
#print(url)
resp = requests.get(url)
#print(resp)
soup = BeautifulSoup(resp.text)
#print(soup)
llist = soup.find_all('div',{'class':'text show-more__control'})
#print(llist)
for x in llist:
    print(x.text)
    print()

index = imdb.get_word_index()
count = 0
sumSentiment = 0
for x in llist:
  x = x.text
  x = ''.join(e for e in x if (e.isalnum() or e==' '))
  #print(x)
  new_x = x.split(' ')
  #print(new_x)
  newest_x = []
  #for z in range(len(test2)):
  newest_x.append([index.get(i,0) + 3 for i in new_x])
  for i in range(len(newest_x[0])):
    if (newest_x[0][i] >= n):
      newest_x[0][i] = 0
  #print(newest_x)
  newest_x = sequence.pad_sequences(newest_x,maxlen=maxlen)
  #print(new_net.predict(newest_x))
  sumSentiment += new_net.predict(newest_x)
  count+=1
print(sumSentiment/count)    





