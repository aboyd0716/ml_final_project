import spacy

def process_string_lemma(input_string):
    nlp = spacy.load("en_core_web_md")
    parsed = nlp(input_string)
    keywords = []
    for sent in parsed.sents:
        for token in sent:
            x = token.pos_
            if x is not 'DET' and x is not 'ADP' and x is not 'PUNCT' and x is not 'SPACE' and x is not 'ADV' and x is not 'ADP':
                keywords.append(str(token.lemma_))
    return(' '.join(keywords))

def process_string_token(input_string):
    nlp = spacy.load("en_core_web_md")
    parsed = nlp(input_string)
    keywords = []
    for sent in parsed.sents:
        for token in sent:
            x = token.pos_
            if x is not 'DET' and x is not 'ADP' and x is not 'PUNCT' and x is not 'SPACE' and x is not 'ADV' and x is not 'ADP':
                keywords.append(str(token))
    return(' '.join(keywords))