Final Code Base for Our Project:

/
	-> Contains all files necessary to run the program, running 
	gui.py, will initialize all of them

/assets

	-> Contains all necessary images, datasets, rnnModels...etc

/assets/extra_code
	
	-> Contains all code that was necessary to the project, but
	not necessary to every run as well as a giant notebook dump

Python Environment Packages / Dependencies:

absl-py==0.8.1
astor==0.8.0
beautifulsoup4==4.8.1
blis==0.4.1 
bs4==0.0.1 
catalogue==0.0.8 
certifi==2019.9.11 
chardet==3.0.4 
cymem==2.0.3 
en-core-web-md==2.2.5 
gast==0.3.2 
google-pasta==0.1.8 
grpcio==1.25.0 
h5py==2.10.0 
idna==2.8 
importlib-metadata==0.23 
joblib==0.14.0 
Keras-Applications==1.0.8 
Keras-Preprocessing==1.1.0 
Markdown==3.1.1 
more-itertools==7.2.0 
murmurhash==1.0.2 
numpy==1.17.4 
pandas==0.25.3 
pkg-resources==0.0.0 
plac==1.1.3 
preshed==3.0.2 
protobuf==3.11.0 
python-dateutil==2.8.1 
pytz==2019.3 
requests==2.22.0 
scikit-learn==0.21.3 
scipy==1.3.3 
six==1.13.0 
sklearn==0.0 
soupsieve==1.9.5 
spacy==2.2.3 
srsly==0.2.0 
tensorboard==1.14.0 
tensorflow==1.14.0 
tensorflow-estimator==1.14.0 
termcolor==1.1.0 
thinc==7.3.1 
tqdm==4.39.0 
urllib3==1.25.7 
wasabi==0.4.0 
Werkzeug==0.16.0
wrapt==1.11.2 
zipp==0.6.0
